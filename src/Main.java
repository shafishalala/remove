import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(11);
        numbers.add(12);
        numbers.add(9);
        numbers.add(44);
        numbers.add(27);

        Iterator<Integer> iterator = numbers.iterator();
        while (iterator.hasNext()) {
            int num = iterator.next();
            if (num % 3 == 0) {
                iterator.remove();
            }
        }

        System.out.println("NUmbers whichs not divisible by 3: " + numbers);
    }
}